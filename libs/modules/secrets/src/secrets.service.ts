import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SecretsService extends ConfigService {
  constructor() {
    super();
  }

  alertApi = {
    port: this.get('ALERTING_APP_PORT'),
  };

  smsApi = {
    port: this.get('SMS_APP_PORT'),
  };

  emailApi = {
    port: this.get('EMAIL_APP_PORT'),
  };

  kafka = {};

  database = {};
}
