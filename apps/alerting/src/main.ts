import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SecretsService } from 'lib/secrets';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const { alertApi } = app.get(SecretsService);

  await app.listen(alertApi.port, () => {
    Logger.log(`Alerting is Listening on port ${alertApi.port}`, 'Bootstrap');
  });
}
bootstrap();
