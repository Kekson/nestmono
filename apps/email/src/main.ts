import { NestFactory } from '@nestjs/core';
import { EmailModule } from './email.module';
import { SecretsService } from 'lib/secrets';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(EmailModule);

  const { emailApi } = app.get(SecretsService);
  await app.listen(emailApi.port, () => {
    Logger.log(`Email is Listening on port ${emailApi.port}`, 'Bootstrap');
  });
}
bootstrap();
