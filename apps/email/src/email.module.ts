import { Module } from '@nestjs/common';
import { EmailController } from './email.controller';
import { EmailService } from './email.service';
import { SecretsModule } from 'lib/secrets';

@Module({
  imports: [SecretsModule],
  controllers: [EmailController],
  providers: [EmailService],
})
export class EmailModule {}
