import { NestFactory } from '@nestjs/core';
import { SmsModule } from './sms.module';
import { SecretsService } from 'lib/secrets';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(SmsModule);

  const { smsApi } = app.get(SecretsService);

  await app.listen(smsApi.port, () => {
    Logger.log(`SMS is Listening on port ${smsApi.port}`, 'Bootstrap');
  });
}
bootstrap();
